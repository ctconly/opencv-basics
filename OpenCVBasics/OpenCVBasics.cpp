/*---------------------------------------------------------------------------------------------*//** 
 \file OpenCVBasics.cpp
 \brief A demonstration of basic OpenCV functionality.

 \details 
 This basic OpenCV demo provides examples of how to process images, videos, and used a 
 webcam. It demonstrates color conversion to grayscale, individual pixel access, and the 
 parallelization of this access for videos.\n\n
 You are first presented with a menu to choose whether to work with an individual image file,
 a video, a webcam, or to quit. You are then presented with a menu to choose an operation to apply,
 either inverting the colors, flipping the image in a left-right direction, or flipping the image 
 in an up-down direction. If you are dealing with images, you will be asked if you want to view
 the color or grayscale image. This is to have code to demonstrate color conversion.

 \author Christopher T. Conly
 \version 0.1
--------------------------------------------------------------------------------------------------*/

#include "pch.h"
#include <iostream>
#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std;

/*--------------------------------------------------------------------------------------------*//**
 \typedef Point3_<uint8_t> ColorPixel
 \brief A data type to represent blue, green, and red values of a pixel.
 
 \details
 The ColorPixel type is defined as a shortcut to represent the three values for a pixel in a BGR image 
 (OpenCV's version of RGB--red, green, blue). This type will be used to parallelize the pixel 
 operations while processing video. The parallelization will significantly speed up the code.
 -----------------------------------------------------------------------------------------------*/
typedef cv::Point3_<uint8_t> ColorPixel;

/*--------------------------------------------------------------------------------------------*//**
 \typedef uchar GrayscalePixel
 \brief A data type to represent the value of a grayscale pixel.

 \details
 The GrayscalePixel type is defined an alternate representation of a pixel in a grayscale image. 
 This type will be used to parallelize the pixel operations while processing video. 
 The parallelization will significantly speed up the code.
-----------------------------------------------------------------------------------------------*/
typedef uchar GrayscalePixel;

void displayMainMenu();
void displayOpMenu();
void displayColorMenu();
int  getChoice(void(*menuFunc)());
void processImage(int op);
void processVideo(int op);
void processWebcam(int op);
void invertColorPixel(ColorPixel &src);
cv::Mat invertColors(cv::Mat img);
cv::Mat invertColorsParallel(cv::Mat img);
cv::Mat flipLR(cv::Mat img);
cv::Mat flipUD(cv::Mat img);
cv::Mat flipDiag(cv::Mat img);

/*---------------------------------------------------------------------------------------------*//**
 \brief Entry and exit point to the program

 \details
 The program first asks if the user wants to work with an image file, a video file, a webcam, or to
 quit. The program will continually loop until the user enters 4 to quit at this menu.\n\n
 
 The user is then presented with a menu asking for the operation to apply to the image/video/webcam 
 frame. It can be to invert the colors, mirror the image left-right, or mirror the image up/down.\n\n
 
 The appropriate operation is applied to the source and when the process begins again.
 
 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
int main()
{
  while (true)
    {
      // Choice to work with image file , video file, webcam, or to quit
      int choice = getChoice(&displayMainMenu);
      
      // Destroy any existing OpenCV windows
      cv::destroyAllWindows();
      
      // Quit
      if (choice == 4)                          
        return(0);

      // Choice sanity check
      else if (choice < 1 || choice > 4)
      {
        printf("Invalid choice.\n");
        continue;
      }

      // A function pointer to the appropriate image source processing function
      void(*process)(int);
      process = (choice == 1) ? &processImage : (choice == 2) ? &processVideo : &processWebcam;

      // Which operation to apply to the image/frames
      int operation = getChoice(&displayOpMenu); 

      // Operation sanity check
      if (operation < 1 || operation > 5)
      {
        cout << "Invalid choice.\n";
        continue;
      }

      // Start the processing
      process(operation);
    }
   
}


/*---------------------------------------------------------------------------------------------*//**
 \brief Inverts colors in an image sequentially, pixel by pixel.

 \details
 This demonstrates the inversion of pixel colors in a sequential manner. It shows how to access and 
 change the value of individual pixels. It processes both single channel grayscale images and the 
 standard OpenCV BGR images.

 \param img a cv::Mat that contains the color image you want to invert
 \return a cv::Mat that is copy of the image with inverted colors

 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
cv::Mat invertColors(cv::Mat img)
{
  // We need a cv::Mat to hold the inverted image
  cv::Mat modified;
  modified.create(img.size(), img.type());
  
  if (img.channels() == 1) // Grayscale image
  {
    for (int row = 0; row < img.rows; row++)
    {
      for (int col = 0; col < img.cols; col++)
      {
        // To access a 1 channel pixel: myMat.at<dataType>(row, col) 
        // or myMat.at<dataType>(cv::Point(col, row))
        modified.at<uchar>(row, col) = 255 - img.at<uchar>(row, col);
      }
    }
  }
  else if (img.channels() == 3) // Color BGR image
  {
    for (int row = 0; row < img.rows; row++)
    {
      for (int col = 0; col < img.cols; col++)
      {
        // We need to grab all three channels. cv::Vec3b is a vector of 
        // 3 byte-sized values (blue-green-red)
        cv::Vec3b origValues = img.at<cv::Vec3b>(row, col);
        cv::Vec3b modValues = modified.at<cv::Vec3b>(row, col);
        
		    // Invert a channel by subtracting its value from 255, so 0 becomes 255 and 255 becomes 0
		    modValues[0] = 255 - origValues[0]; 
        modValues[1] = 255 - origValues[1];
        modValues[2] = 255 - origValues[2];
		
        // Assign the BGR values to the image pixel
        modified.at<cv::Vec3b>(row, col) = modValues;
      }
    }
  }

  return modified;
}


/*---------------------------------------------------------------------------------------------*//**
 \brief Inverts colors in a cv::Mat in a parallel manner.

 \details
 This demonstrates the inversion of pixel colors in a parallel manner using the cv::Mat.forEach(...)
 method and a function operator. I might change this to use a lamda function instead. 
 See \ref invertPixel for the individual pixel inversion function to be parallelized.

 \param img a cv::Mat containing the color image you want to invert
 \return a cv::Mat that is copy of the image with inverted colors

 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
cv::Mat invertColorsParallel(cv::Mat img)
{
  // Create the operator that the parallelized forEach will use to work on pixels simultaneously
  struct InvertOperator
  {
    // Operator to work on color pixels
    void operator ()(ColorPixel &src, const int *position) const
    {
      invertColorPixel(src); // Calls this to invert a single pixel. See definition below.
    }

    // Operator to work on grayscale pixels
    void operator ()(GrayscalePixel &src, const int *position) const
    {
      src = 255 - src;
    }
  };

  // Create a copy of the current image to operate on
  cv::Mat modified;
  img.copyTo(modified);
  
  // This is the forEach that will call the operator (and thus the inversion function) on 
  // each image pixel in parallel. The OpenCV authors have taken care of the parallelism.
  // It can't be used for an operation that requires the value of any other pixel.
  if (modified.channels() == 3)
    modified.forEach<ColorPixel>(InvertOperator());
  else if (modified.channels() == 1)
    modified.forEach<GrayscalePixel>(InvertOperator());

  return modified;
}


/** ------------------------------------------------------------------------------------------------
 \brief A single pixel inversion function.
 
 \details This function is used in the parallelized code to invert the color of an individual pixel. 
 \param pixel a reference to the pixel to be inverted (See the Point3_<uint8_t> Pixel typedef).
 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
void invertColorPixel(ColorPixel &pixel)
{
  pixel.x = 255 - pixel.x;
  pixel.y = 255 - pixel.y;
  pixel.z = 255 - pixel.z;
}


/*---------------------------------------------------------------------------------------------*//**
 \brief Mirrors an image left-right.

 \details
 This demonstrates creating a mirror image. We do not use OpenCV's cv::flip function in order to
 shows how to access and change the value of individual pixels. It processes both single channel 
 grayscale images and the standard OpenCV BGR images.

 \param img a cv::Mat that contains the color image you want to mirror
 \return a cv::Mat that is copy of the image that is mirrored left to right
 \todo actually write the code

 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
cv::Mat flipLR(const cv::Mat img)
{
  // Create the operator that the parallelized forEach will use to work on pixels simultaneously
  struct FlipLROperator
  {
    const cv::Mat orig;
    FlipLROperator(const cv::Mat& m) : orig(m) { }

    // Operator to work on color pixels
    void operator ()(ColorPixel &dst, const int *position) const
    {
      int r = position[0];
      int c = orig.cols - position[1] - 1;
      cv::Vec3b values = orig.at<cv::Vec3b>(r, c);
      dst.x = values[0];
      dst.y = values[1];
      dst.z = values[2];
    }

    // Operator to work on grayscale pixels
    void operator ()(GrayscalePixel &dst, const int *position) const
    {
      int r = position[0];
      int c = orig.cols - position[1] - 1;
      dst = orig.at<uchar>(r, c);
    }
  };

  cv::Mat modified;
  modified.create(img.size(), img.type());
  if (modified.channels() == 3)
    modified.forEach<ColorPixel>(FlipLROperator(img));
  else if (modified.channels() == 1)
    modified.forEach<uchar>(FlipLROperator(img));

  return modified;
}


/*---------------------------------------------------------------------------------------------*//**
 \brief Mirrors an image up-down.

 \details
 This demonstrates creating a mirror image. We do not use OpenCV's cv::flip function in order to
 shows how to change the value of individual pixels in parallel. It processes standard BGR images.

 \param img a cv::Mat that contains the color image you want to mirror
 \return a cv::Mat that is copy of the image that is mirrored top to bottom
 \todo actually write the code

 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
cv::Mat flipUD(cv::Mat img)
{
  // Create the operator that the parallelized forEach will use to work on pixels simultaneously.
  // We need a copy of the original image, so we have a constructor that accounts for this.
  struct FlipUDOperator
  {
    const cv::Mat orig;
    FlipUDOperator(const cv::Mat& m) : orig(m) { }

    // Operator to work on color pixels
    void operator ()(ColorPixel &dst, const int *position) const
    {
      int r = orig.rows - position[0] - 1;
      int c = position[1];
      cv::Vec3b values = orig.at<cv::Vec3b>(r, c);
      dst.x = values[0];
      dst.y = values[1];
      dst.z = values[2];
    }

    // Operator to work on grayscale pixels
    void operator ()(GrayscalePixel &dst, const int *position) const
    {
      int r = orig.rows - position[0] - 1;
      int c = position[1];
      dst = orig.at<GrayscalePixel>(r, c);
    }
  };

  cv::Mat modified;
  modified.create(img.size(), img.type());
  if (modified.channels() == 3)
    modified.forEach<ColorPixel>(FlipUDOperator(img));
  else if (modified.channels() == 1)
    modified.forEach<GrayscalePixel>(FlipUDOperator(img));
  return modified;
}


/*---------------------------------------------------------------------------------------------*//**
 \brief Mirrors an image left-right and up-down.

 \details
 This demonstrates creating a mirror image. We do not use OpenCV's cv::flip function in order to
 shows how to change the value of individual pixels in parallel. It processes standard BGR images.

 \param img a cv::Mat that contains the color image you want to mirror
 \return a cv::Mat that is copy of the image that is mirrored left to right and up to down

 \author Christopher T. Conly
 --------------------------------------------------------------------------------------------------*/
cv::Mat flipDiag(const cv::Mat img)
{
  // Create the operator that the parallelized forEach will use to work on pixels simultaneously.
  // We need a copy of the original image, so we have a constructor that accounts for this.
  struct FlipDiagOperator
  {
    const cv::Mat orig;
    FlipDiagOperator(const cv::Mat& m) : orig(m) { }

    // Operator to work on color pixels
    void operator ()(ColorPixel &dst, const int *position) const
    {
      int r = orig.rows - position[0] - 1;
      int c = orig.cols - position[1] - 1;
      cv::Vec3b values = orig.at<cv::Vec3b>(r, c);
      dst.x = values[0];
      dst.y = values[1];
      dst.z = values[2];
    }

    // Operator to work on grayscale pixels
    void operator ()(GrayscalePixel &dst, const int *position) const
    {
      int r = orig.rows - position[0] - 1;
      int c = orig.cols - position[1] - 1;
      dst = orig.at<GrayscalePixel>(r, c);
    }
  };

  cv::Mat modified;
  modified.create(img.size(), img.type());
  if (modified.channels() == 3)
    modified.forEach<ColorPixel>(FlipDiagOperator(img));
  else if (modified.channels() == 1)
    modified.forEach<GrayscalePixel>(FlipDiagOperator(img));
  return modified;
}


/*---------------------------------------------------------------------------------------------*//**
 \brief Detects edges using custom kernels. Really just runs and combines two filters.

 \details
 This demonstrates filtering with custom kernels. It processes grayscale images.

 \param src a cv::Mat that contains the grayscale image you want to find edges in
 \param vert_kernel a kernel to detect vertical edges
 \param horiz_kernel a kernel to detect horizontal edges
 \return a cv::Mat that contains the edges

 \author Christopher T. Conly
 --------------------------------------------------------------------------------------------------*/
cv::Mat detectEdges(cv::Mat src, cv::Mat vert_kernel, cv::Mat horiz_kernel)
{
  cv::Mat horizontal; // holds the horizontal edges
  cv::Mat vertical;   // Holds the vertical edges
  cv::Mat edges;      // Holds the combined vertical and horizontal edges
  cv::Mat output;      // Holds the final thresholded edge image;

  // Create the edge matrices
  horizontal.create(src.size(), CV_16S);
  vertical.create(src.size(), CV_16S);
  
  // Generate the edges using the filter2D function to convolve the image with our kernels
  cv::filter2D(src, horizontal, -1, horiz_kernel);
  cv::filter2D(src, vertical, -1, vert_kernel);

  // Take the absolute value of each element so we collect both the positive and negative edges. 
  // Also, add the vertical and horizontal edges together into a single edge matrix, called edges.
  cv::add(cv::abs(horizontal), cv::abs(vertical), edges);

  // Convert our edge matrix from 16 bit signed to 8 bit unsigned (i.e. an image)
  output.create(horizontal.size(), CV_8UC1);
  edges.convertTo(output, CV_8U);

  // Set any pixel less than threshold to 0, so the image isn't too noisy
  int threshold = 90;    // Our threshold below which pixels will be set to 0
  int thresholdType = 3;  // Type 3 ("Threshold to Zero") means anything less than threshold is set to 0
  cv::threshold(output, output, threshold, 255, thresholdType);

  return output;
}


/*---------------------------------------------------------------------------------------------*//**
 \brief Entry point to generating an edge image from a source image.

 \details
 Creates the custom kernels, converts the image to grayscal and calls our edge detection function.

 \param src a cv::Mat that contains the color image you want to find edges in
 \return a cv::Mat image that contains the edges

 \author Christopher T. Conly
 --------------------------------------------------------------------------------------------------*/
cv::Mat generateEdgeImage(cv::Mat src)
{
  cv::Mat grayscale;
  cv::cvtColor(src, grayscale, cv::COLOR_BGR2GRAY);

  /* Generate our vertival and horizontal kernels
     
     Vertical:
     --------------
     | 1 | 0 | -1 |
     --------------
     | 1 | 0 | -1 |
     --------------
     | 1 | 0 | -1 |
     --------------

     Horizontal:
     ----------------
     |  1 |  1 |  1 |
     ----------------
     |  0 |  0 |  0 |
     ----------------
     | -1 | -1 | -1 |
     ----------------
  */
  cv::Mat vertKernel = cv::Mat::ones(3, 3, CV_8SC1);
  for (int r = 0; r < vertKernel.rows; r++)
  {
    vertKernel.at<uchar>(r, 2) = -1;
    vertKernel.at<uchar>(r, 1) = 0;
  }

  cv::Mat horizKernel = cv::Mat::ones(3, 3, CV_8SC1);
  for (int c = 0; c < horizKernel.cols; c++)
  {
    horizKernel.at<uchar>(1, c) = 0;
    horizKernel.at<uchar>(2, c) = -1;
  }

  return detectEdges(grayscale, vertKernel, horizKernel);
}

/*---------------------------------------------------------------------------------------------*//**
 \brief Mirrors an image up-down.

 \details
 This demonstrates creating a mirror image. We do not use OpenCV's cv::flip function in order to
 shows how to access and change the value of individual pixels. It processes both single channel
 grayscale images and the standard OpenCV BGR images.

 \param img a cv::Mat that contains the color image you want to mirror
 \return a cv::Mat that is copy of the image that is mirrored top to bottom
 \todo actually write the code

 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
void processImage(int op)
{
  // Get the image file name
  cout << "Enter image name (can't be a gif): ";
  string filename;
  cin >> filename;

  // Read the image and make sure we read it successfully
  cv::Mat original = cv::imread(filename);
  if (original.empty())
  {
    cout << "Error opening file " << filename << endl;
    return;
  }

  // Make a grayscale copy to demonstrate processing of single channel images
  cv::Mat grayscale;
  cv::cvtColor(original, grayscale, CV_BGR2GRAY); // Converts it from BGR (blue-green-red) to grayscale

  cv::Mat modified;   // the inverted image
  cv::Mat unmodified; // either the color or grayscale original image
  unmodified = (getChoice(&displayColorMenu) == 1) ? original : grayscale;

  // Call the appropriate modification operation
  if (op == 1)
    modified = invertColors(unmodified);
  else if (op == 2)
    modified = flipLR(unmodified);
  else if (op == 3)
    modified = flipUD(unmodified);
  else if (op == 4)
    modified = flipDiag(unmodified);
  else if (op == 5)
    modified = generateEdgeImage(original);

  cv::namedWindow("Original", cv::WINDOW_AUTOSIZE);
  cv::namedWindow("Original Modified", cv::WINDOW_AUTOSIZE);
  cv::imshow("Original", unmodified);
  cv::imshow("Original Modified", modified);
  cv::destroyAllWindows(); // Destroy GUI windows
}


/*---------------------------------------------------------------------------------------------*//**
 \brief Starting point to process a video file.

 \details
 This sets up OpenCV to process a video file as opposed to a single image file or wecam video.

 \param op an int that represents the operation to perform on each video frame
 \todo actually write the code

 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
void processVideo(int op)
{
  // Get the video name and try to open it. Print error message if we can't open it.
  cout << "Enter video name: ";
  string filename;
  cin >> filename;
  cv::VideoCapture cap = cv::VideoCapture(filename); // only difference from reading from a webcam
  if (!cap.isOpened())
  {
    printf("Error opening video file.\n\n");
    return;
  }

  // We can access information about the video
  int frameWidth = (int) cap.get(CV_CAP_PROP_FRAME_WIDTH);   // Resolution width
  int frameHeight = (int) cap.get(CV_CAP_PROP_FRAME_HEIGHT); // Resolution height
  int numFrames = (int) cap.get(CV_CAP_PROP_FRAME_COUNT);    // Number of frames in the video
  double fps = cap.get(CV_CAP_PROP_FPS);                     // Frame rate of the video
  printf("\nProcessing %s\nVideo height: %d\nVideo width: %d\nNumber of frames: %d\nFPS: %.1f\n\n", filename, frameHeight, frameWidth, numFrames, fps);

  // Frame will hold the current frame. We also need windows to display the unmodified and modified frames.
  cv::Mat frame;
  cv::namedWindow("Original", cv::WINDOW_AUTOSIZE);
  cv::namedWindow("Modified", cv::WINDOW_AUTOSIZE);
  
  for (int i = 0; i < numFrames; i++) // We have a fixed number of frames
  {
    cap >> frame;     // Get a frame from the video
    cv::Mat modified; // We need a cv::Mat for the modified frame.
    
    // Call the appropriate modification operation
    if (op == 1)
      modified = invertColorsParallel(frame);
    else if (op == 2)
      modified = flipLR(frame);
    else if (op == 3)
      modified = flipUD(frame);
    else if (op == 4)
      modified = flipDiag(frame);
    else if (op == 5)
      modified = generateEdgeImage(frame);

    // Display the original and modified frames
    cv::imshow("Original", frame);
    cv::imshow("Modified", modified);

    // Quit if user to presses a key. This also gives OpenCV the chance to display the frame
    if (cv::waitKey(15) >= 0)
      break;
  }

  cap.release();           // Release the video capture object
  cv::destroyAllWindows(); // Destroy GUI windows
}


/*---------------------------------------------------------------------------------------------*//**
 \brief Starting point to process webcam video.

 \details
 This sets up OpenCV to process webcam video as opposed to a single image file or a video file.

 \param op an int representing the operation to perform on each video frame

 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
void processWebcam(int op)
{
  // Change the integer value to change webcams on a system with multiple webcams
  cv::VideoCapture cap = cv::VideoCapture(0); // only difference from reading from a video
  if (!cap.isOpened()) // make sure it opened successfully
  {
    printf("Error opening webcam.\n\n");
    return;
  }

  // We can access information about the webcam
  int frameWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH);   // Resolution width
  int frameHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT); // Resolution height
  int numFrames = cap.get(CV_CAP_PROP_FRAME_COUNT);    // Number of frames in the video. This will be -1 for a webcam.
  double fps = cap.get(CV_CAP_PROP_FPS);               // Frame rate of the video
  printf("\nProcessing webcam\nVideo height: %d\nVideo width: %d\nNumber of frames: %d (-1: webcam)\nFPS: %.1f\n\n", frameHeight, frameWidth, numFrames, fps);

  // Frame will hold the current frame. We also need windows to display the unmodified and modified frames.
  cv::Mat frame;
  cv::namedWindow("Original", cv::WINDOW_AUTOSIZE);
  cv::namedWindow("Modified", cv::WINDOW_AUTOSIZE);
  
  for (;;) // Infinite loop, since we don't know how long the user wants the webcam footage to play
  {
    cap >> frame;     // Get a frame from the video
    cv::Mat modified; // We need a cv::Mat for the modified frame.

    // Call the appropriate modification operation
    if (op == 1)
      modified = invertColorsParallel(frame);
    else if (op == 2)
      modified = flipLR(frame);
    else if (op == 3)
      modified = flipUD(frame); 
    else if (op == 4)
      modified = flipDiag(frame);
    else if (op == 5)
      modified = generateEdgeImage(frame);
 
    // Display the original and modified frames
    cv::imshow("Original", frame);
    cv::imshow("Modified", modified);

    // Quit if user to presses a key. This also gives OpenCV the chance to display the frame
    if (cv::waitKey(15) >= 0)
      break;
  }

  cap.release();           // Release the video capture object
  cv::destroyAllWindows(); // Destroy GUI windows
}


/*---------------------------------------------------------------------------------------------*//**
 \brief Displays the main menu to choose what to work with.

 \details
 This asks the user to choose whether to work with an image file, a video file, webcam video, 
 or to quit the program.

 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
void displayMainMenu()
{
  printf("\n");
  printf("OpenCV Basics Menu\n");
  printf("------------------\n");
  printf("1. Image\n2. Video\n3. Webcam\n4. Quit\n");
  printf("------------------\n\n");
}


/*---------------------------------------------------------------------------------------------*//**
 \brief Displays the menu to choose what operation to apply to the image/video/webcam frames.

 \details
 This asks the user to choose whether to invert colors, flip left-right, or flip up-down.

 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
void displayOpMenu()
{
  printf("\n");
  printf("OpenCV Operations Menu\n");
  printf("----------------------\n");
  printf("1. Invert Colors\n2. Flip Left/Right\n3. Flip Up/Down\n4. Flip Diagonally\n5. Detect edges\n");
  printf("----------------------\n\n");
}

/*---------------------------------------------------------------------------------------------*//**
 \brief Displays the menu to choose whether to process color or grayscale image/frame.

 \details
 This asks the user to choose whether to use the original color or grayscale image.

 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
void displayColorMenu()
{
  printf("\n");
  printf("View color or grayscale ?\n");
  printf("-------------------------\n");
  printf("1. Color\n2. Grayscale\n");
  printf("-------------------------\n\n");
}


/*---------------------------------------------------------------------------------------------*//**
 \brief Displays the appropriate menu and gets a choice from the user.

 \details
 This asks the user to choose whether to invert colors, flip left-right, or flip up-down.

 \param[in] menuFunc a pointer to the appropriate menu display function
 \param[out] an int representing the user's choice

 \author Christopher T. Conly
--------------------------------------------------------------------------------------------------*/
int getChoice(void(*menuFunc)())
{
  int choice;
  menuFunc();
  printf("Choice: ");
  cin >> choice;
  return choice;
}
