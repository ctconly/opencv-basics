A demonstration of basic OpenCV usage.
Reading and writing images.
Reading and writing videos.
Using webcams.
cv::Mat structure
cv::Rect structure for ROI
cv::Point structure
Accessing and modifying individual pixels
Parallel pixel access
Various simple cv::Mat operations
